This repository retrieves the information about TheoretiCS submissions from the
Episciences API, to be used to compose digests.

It can only be used by the TheoretiCS team and requires authentication.

To use it, you should either answer the questions about Episciences credentials,
or have a file `.dashboard.ini` in the present repository containing variables
`THEORETICS_USER` and `THEORETICS_PW` with your Episciences credentials.

The code is adapted from the [Epi RC](https://git8.cs.fau.de/software/epirc)
project.

The code is licenced under GPLv3 -- see LICENSE.

