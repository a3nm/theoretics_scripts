#!/usr/bin/python3

import sqlite3
from copy import deepcopy

con = sqlite3.connect('dashboard.db')
cur = con.cursor()

PAPER_EVENTS = [
    'desk_reject'
    'assign_to_phase1'
    'set_paper_handling_editor'
    'set_paper_partner_editor'
    'email_paper_handling_editor'
    'email_paper_partner_editor'
    'handling_editor_has_coi'
    'partner_editor_has_coi'
    'handling_editor_no_coi'
    'partner_editor_no_coi'
    'set_paper_recommendation_deadline'
    'email_authors_about_editor'
    'make_recommendation'
    'reject'
    'accept_to_phase2'
]

PAPER_TRANSITIONS = [
    ('eic_assignment',
        [],
        [['assign_to_phase1'], ['desk_reject']]),
    ('handling_editor_assignment', 
        [['assign_to_phase1'], ['handling_editor_has_coi']],
        [['set_paper_handling_editor', 'email_paper_handling_editor']]),
    ('partner_editor_assignment', 
        [['assign_to_phase1'], ['partner_editor_has_coi']],
        [['set_paper_partner_editor', 'email_paper_partner_editor']]),
    ('handling_editor_confirmation',
        [['set_paper_handling_editor', 'email_paper_handling_editor']],
        [['handling_editor_has_coi'], ['handling_editor_no_coi']]),
    ('partner_editor_confirmation',
        [['set_paper_partner_editor', 'email_paper_partner_editor']],
        [['partner_editor_has_coi'], ['partner_editor_no_coi']]),
    ('set_paper_recommendation_deadline',
        [['handling_editor_no_coi', 'partner_editor_no_coi']],
        [['set_paper_recommendation_deadline', 'email_authors_about_editor']]),
    ('reviewing', 
        [['set_paper_recommendation_deadline', 'email_authors_about_editor']],
        [['REVIEWS_BEGIN']]),
    ('recommendation', 
        [['REVIEWS_END']],
        [['make_recommendation']]),
    ('verdict', 
        [['make_recommendation']],
        [['reject'], ['accept_to_phase2']]),
]

EICS = {}
cureic = con.cursor()
for eic in cureic.execute("SELECT id, alias, name, email FROM Editor_in_chief"):
    EICS[eic[0]] = eic[1:]

EDITORS = {}
cured = con.cursor()
for editor in cured.execute("SELECT id, alias, name, email FROM Editor"):
    EDITORS[editor[0]] = editor[1:]

def create_paper(paperid, name, authors, date):
    cur.execute("INSERT INTO Paper(id, title, authors, submission_date) VALUES (?, ?, ?, ?)", (paperid, name, authors, date))

def copy_transition(transition):
    return (transition[0], deepcopy(transition[1]), deepcopy(transition[2]))

def add_event(paper, event_type):
    cur.execute("INSERT INTO Paper_events(event_date, event_import_date, paper, event_type) VALUES (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, ?, ?)", (paper, event_type))

def dashboard():
    for raw_paper in cur.execute('SELECT id, title, authors, submission_date FROM paper ORDER BY id'):
        paper = {}
        paper['id'] = raw_paper[0]
        paper['title'] = raw_paper[1]
        paper['authors'] = raw_paper[2]
        paper['submission_date'] = raw_paper[3]
        # open transitions and date of last action
        open_transitions = [(copy_transition(PAPER_TRANSITIONS[0]), paper['submission_date'])]
        open_events = []
        cur2 = con.cursor()
        for event in cur2.execute(
                "SELECT event_date, event_type, event_data FROM Paper_event WHERE paper = ? ORDER BY event_date", (str(paper['id']), )):
            # check event is admissible
            ok = False
            for (transition, last_date) in open_transitions:
                for outcome in transition[2]:
                    if event[1] in outcome:
                        ok = True
            if not ok:
                print ("WARNING: unexpected event! %s %s %s" % event)

            # add event to queue
            open_events.append(event)

            # handle event semantics
            if event[1] == 'desk_reject':
                paper['status'] = 'rejected'
            elif event[1] == 'assign_to_phase1':
                paper['phase'] = 1
            elif event[1] == 'set_paper_handling_editor':
                paper['handling_editor'] = EDITORS[int(event[2])][0] + '?'
            elif event[1] == 'set_paper_partner_editor':
                paper['partner_editor'] = EDITORS[int(event[2])][0] + '?'
            elif event[1] == 'handling_editor_has_coi':
                paper['handling_editor'] = "???"
            elif event[1] == 'partner_editor_has_coi':
                paper['partner_editor'] = "???"
            elif event[1] == 'set_paper_recommendation_deadline':
                paper['recommendation_deadline'] = event[2]
            elif event[1] == 'make_recommendation':
                paper['recommendation'] = event[2]
            elif event[1] == 'reject':
                paper['status'] = 'rejected'
            elif event[1] == 'accept_to_phase2':
                paper['phase'] = 2

            # check completed transitions
            new_open_transitions = []
            for (transition, last_date) in open_transitions:
                done = False
                progress = False
                for outcome in transition[2]:
                    # check if transition is complete
                    if set(outcome) <= set([event[1] for event in open_events]):
                        # transition is done
                        done = True
                    # check if event helps transition
                    if event in outcome:
                        progress = True
                if not done:
                    if not progress:
                        new_open_transitions.append((transition, last_date))
                    else:
                        # transition updated to time of event
                        new_open_transitions.append((transition, event[2]))
                else:
                    pass # remove transition
            open_transitions = new_open_transitions

            # check new transitions
            changed = True
            while changed:
                changed = False
                # open new transitions
                for transition in PAPER_TRANSITIONS[1:]:
                    for trigger in transition[1]:
                        if set(trigger) <= set([event[1] for event in open_events]):
                            # open transition and drop events
                            changed = True
                            new_events = [e for e in open_events if e[1] not in
                                    trigger]
                            open_events = new_events
                            open_transitions.append((copy_transition(transition),
                                event[2]))

        print (paper)
        print (open_transitions, open_events)

                    


dashboard()

