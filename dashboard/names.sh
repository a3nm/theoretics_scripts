#!/bin/bash

echo "INSERT INTO Editor_in_chief(alias, name) VALUES ('j', 'Javier Esparza'), ('u', 'Uri Zwick');"
cat editors.txt | cut -f1 | while read l; do
ALIAS=$(echo "$l" | rev | cut -d' ' -f1 | rev | tr '[A-Z]' '[a-z]' | sed 's/å/a/g')
NAME="$l"
# SQL injection possible, only use on trusted data
echo "INSERT INTO Editor(alias, name) VALUES ('$ALIAS', '$NAME');"
done



