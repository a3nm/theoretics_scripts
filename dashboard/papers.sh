cat papers.tsv| while read l; do
  ID=$(echo "$l" |cut -f1)
  TITLE=$(echo "$l" |cut -f2)
  AUTHORS=$(echo "$l" |cut -f3)
  DATE=$(echo "$l" |cut -f4)
  # SQL injection possible, only use on trusted data
  echo "INSERT INTO Paper(id, title, authors, submission_date) VALUES ('$ID', '$TITLE', '$AUTHORS', '$DATE');"
done
