PRAGMA foreign_keys = ON;

CREATE TABLE Editor_in_chief(
  id INTEGER PRIMARY KEY,
  alias TEXT,
  name TEXT,
  email TEXT
);

CREATE TABLE Editor(
  id INTEGER PRIMARY KEY,
  alias TEXT,
  name TEXT,
  email TEXT
);

CREATE TABLE Reviewer(
  id INTEGER PRIMARY KEY,
  name TEXT,
  email TEXT
);

CREATE TABLE Paper(
  id INTEGER PRIMARY KEY,
  title TEXT,
  authors TEXT,
  remark TEXT,
  submission_date DATE
);

CREATE TABLE Review(
  id INTEGER PRIMARY KEY,
  paper INTEGER,
  reviewer INTEGER,
  FOREIGN KEY(paper) REFERENCES Paper(id),
  FOREIGN KEY(reviewer) REFERENCES Reviewer(id)
);

CREATE TABLE Paper_event(
  id INTEGER PRIMARY KEY,
  event_date DATE,
  event_import_date DATE,
  paper INTEGER,
  event_type TEXT,
  event_data TEXT,
  FOREIGN KEY(paper) REFERENCES Paper(id)
);

CREATE TABLE Paper_followup(
  id INTEGER PRIMARY KEY,
  event_date DATE,
  event_import_date DATE,
  paper INTEGER,
  event_type TEXT,
  remark TEXT,
  new_deadline DATE,
  FOREIGN KEY(paper) REFERENCES Paper(id)
);

CREATE TABLE Review_event(
  id INTEGER PRIMARY KEY,
  event_date DATE,
  event_import_date DATE,
  review INTEGER,
  event_type TEXT,
  event_data TEXT,
  FOREIGN KEY(review) REFERENCES Review(id)
);

CREATE TABLE Review_followup(
  id INTEGER PRIMARY KEY,
  event_date DATE,
  event_import_date DATE,
  review INTEGER,
  event_type TEXT,
  remark TEXT,
  new_deadline DATE,
  FOREIGN KEY(review) REFERENCES Review(id)
);

CREATE TABLE Paper_status(
  paper INTEGER,
  editor_in_chief INTEGER,
  handling_editor INTEGER,
  partner_editor INTEGER,
  phase INTEGER,
  status_text TEXT,
  FOREIGN KEY(paper) REFERENCES Paper(id),
  FOREIGN KEY(editor_in_chief) REFERENCES Editor_in_chief(id),
  FOREIGN KEY(handling_editor) REFERENCES Editor(id),
  FOREIGN KEY(partner_editor) REFERENCES Editor(id)
);
