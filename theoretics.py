#!/usr/bin/env python3

# Retrieve paper information for TheoretiCS digests

# adapted from epirc by epirc contributors
# https://git8.cs.fau.de/software/epirc
# license: GPLv3

import requests
import os
import re
import sys
import json
from html.parser import HTMLParser
# https://stackoverflow.com/a/16115575
from dateutil.parser import parse as dateutil_parse
# https://stackoverflow.com/a/4406260
from datetime import date
from dateutil.relativedelta import relativedelta

g_dir = os.path.dirname(os.path.realpath(__file__))
g_cookie_filename = ".cookies.json"
g_cookie_file = os.path.join(g_dir, g_cookie_filename)
iniFile = {}
g_ini_filename = '.dashboard.ini'
g_ini_file = os.path.join(g_dir, g_ini_filename)

user_agent = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:21.0) Gecko/20100101 Firefox/21.0'}

def loadIniFile():
    global iniFile
    try:
        print(g_ini_file)
        with open(g_ini_file, 'r') as rcfile:
            proper_line = re.compile('^[^#]')
            for l in rcfile.readlines():
                if not proper_line.match(l):
                    continue
                linetokens = l.strip('\r\n').split('=', 1)
                if len(linetokens) != 2:
                    print("Skipping invalid line »%s«" % l.strip('\r\n'))
                    continue
                iniFile[linetokens[0]] = linetokens[1]
    except FileNotFoundError:
        print('\'.dashboard.ini\' not found, skipping')

def env_or_user(var_name, is_passwd=False):
    global iniFile
    if var_name in os.environ:
        return os.environ[var_name]
    elif var_name in iniFile:
        return iniFile[var_name]
    else:
        print('Variable {} not in env or ini file, please enter: '.format(var_name), end='', flush=True)
        if is_passwd:
            return getpass.getpass()
        else:
            return sys.stdin.readline().strip()

def get_episciences_session(rvcode):
    """ login into episciences (if necessary) and journal session"""

    if hasattr(get_episciences_session, 'session'):
        return get_episciences_session.session
    # else authenticate

    my_session = requests.Session()
    try:
        with open(g_cookie_file, "r") as fh:
            my_session.cookies = requests.utils.cookiejar_from_dict(json.load(fh))
    except FileNotFoundError:
        pass
    except PermissionError:
        pass

    url = 'https://cas.ccsd.cnrs.fr/cas/login'

    getparams = {
        'locale': 'en',
        'service': 'https://%s.episciences.org/user/login/forward-controller/index/forward-action/index' % rvcode
    }

    r = my_session.get(url, params = getparams)

    # create a subclass and override the handler methods
    class FormExtractor(HTMLParser):
        def __init__(self):
            super(FormExtractor,self).__init__()
            self.fields = {}
            self.action = ""
            self.inCredentials = False

        def handle_starttag(self, tag, attrs):
            if tag == "form":
                attrs = dict(attrs)
                if 'id' in attrs and attrs['id'] == 'credential':
                    self.action = attrs.get('action', '/')
                    self.inCredentials = True
            if tag == "input" and self.inCredentials:
                attrs = dict(attrs)
                if 'name' in attrs and 'value' in attrs:
                    name = attrs['name']
                    value = attrs['value']
                    self.fields[name] = value
                    #print("{} --> {}".format(name, value))
                    #print("Encountered a start tag:" + tag)

        def handle_endtag(self, tag):
            #print("Encountered an end tag :" + tag)
            if tag == 'form' and self.inCredentials:
                self.inCredentials = False

        def handle_data(self, data):
            #print("Encountered some data  :" + data)
            pass

    # instantiate the parser and feed it some HTML
    parser = FormExtractor()
    parser.feed(r.text)

    authenticated = False
    # if there has been no <form>, then there is no reason to authenticate
    if parser.action == "":
        # print("Old cookie still valid")
        authenticated = True
    else:
        if not 'jsession' in parser.action:
            print("Discarding old cookie", file=sys.stderr)
            # clear cookies, by starting a new session
            my_session = requests.Session()
            r = my_session.get(url, params = getparams)
            parser = FormExtractor()
            parser.feed(r.text)
        url = 'https://cas.ccsd.cnrs.fr/' + parser.action.split('?')[0]
        postdata = parser.fields
        postdata['username'] = env_or_user('%s_USER' % rvcode.upper())
        postdata['password'] = env_or_user('%s_PW' % rvcode.upper(), is_passwd=True)
        #print ("Submitting to " + url + " with " + str(postdata))
        print('Signing in to {} as {} ... '.format(url, postdata['username']), end='', flush=True, file=sys.stderr)
        r2 = my_session.post(url, params = getparams, data = postdata)
        authenticated = (r2.url == "https://%s.episciences.org/" % rvcode)
        if authenticated:
            print("SUCCESS!")
    #previewHTML(r2.text)
    if authenticated:
        get_episciences_session.session = my_session
        try:
            with open(g_cookie_file, "w") as fh:
                os.chmod(g_cookie_file, 0o600)
                json.dump(requests.utils.dict_from_cookiejar(my_session.cookies), fh, \
                    indent="  ")
        except PermissionError:
            pass
        return my_session
    else:
        print("FAILURE")
        sys.exit(1)

def fix_author_name(a):
    assert (a.count(',') <= 1)
    if a.count(',') == 0:
        return a
    s = a.split(',')
    return (s[0].strip() + ' ' + s[1].strip())

def deadline_from_submission_date(d):
    return d + relativedelta(months=+3)

def print_paper(p, deadline=True):
    global my_session
    detailsr = my_session.get("https://theoretics.episciences.org/%d/json" % p['paperid'], headers=my_headers)
    details = json.loads(detailsr.text)
    authors = [fix_author_name(a) for a in details['authors']]
    submitted = dateutil_parse(details['dateSubmitted'])
    deadline = deadline_from_submission_date(submitted)
    myid = details['paperId']
    assert(len(details['titles']) == 1)
    title = details['titles'][0].replace('\n', '')
    print ("")
    print ("- https://theoretics.episciences.org/administratepaper/view?id=%d"
            % myid)
    print ("  \"%s\"" % title)
    print ("  by %s" % ', '.join(authors))
    if deadline:
        print ("  (submitted on %s => verdict by %s)" % (
                submitted.strftime("%B %d"), deadline.strftime("%B %d")))
    else:
        print ("  (submitted on %s)" % submitted.strftime("%B %d"))
    print ("")

    return myid, authors, title, submitted

PHASES = [(1, "596"), (2, "597")]
GET_PAPERS_OF_VOLUME = "https://theoretics.episciences.org/volume/all/id/%s" 
papers = {}

loadIniFile()
my_session = get_episciences_session("theoretics")
my_headers = {'accept': 'application/json'}

for (phasenum, eid) in PHASES:
    phaser = my_session.get(GET_PAPERS_OF_VOLUME % eid, headers=my_headers)
    phasea = json.loads(phaser.text)
    assert (list(phasea.keys())) == [eid]
    phaseb = phasea[eid]
    assert(sorted(list(phaseb.keys())) == ["id", "name", "papers", "position",
        "status"])
    assert(phaseb["id"] == int(eid))
    assert(phaseb["name"] == "Phase %d" % phasenum)
    papers[phasenum] = phaseb['papers']

print ("=== PHASE 1 ===")
for p in papers[1]:
    print_paper(p)
print ("=== PHASE 2 ===")
for p in papers[2]:
    print_paper(p)



